import json
import time
from typing import List, Optional

from links.redis_pool import RedisPool
from links.utils import get_timestamp_and_unique_key, get_domains_from_urls, get_common_key_part, \
    filter_keys_by_timestamps, not_digits, get_domains_by_timestamps


def test_get_timestamp_and_unique_key():
    """Simple test for simple function."""

    timestamp_start = int(time.time())
    timestamp, unique_key = get_timestamp_and_unique_key()
    assert timestamp_start <= timestamp
    assert len(unique_key) == len(str(timestamp)) + 33  # 32 length of uuid.uuid4().hex + '_'


def test_get_domains_from_urls():
    """Test of get_domains_from_urls function."""

    links = [
        "https://ya.ru",
        "http://ya.ru?q=123",
        " funbox.ru",
        "//www.ya.ru/",
        "/stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor",
        ""
        ]
    domains: List[str] = get_domains_from_urls(links)
    assert len(domains) == 4
    assert set(domains) == {'ya.ru', 'www.ya.ru', 'stackoverflow.com', 'funbox.ru'}


def test_get_common_key_part():
    """Test of get_common_key_part."""

    key1 = "1234567"
    key2 = "1234567"
    common_part: str = get_common_key_part(key1, key2)
    assert common_part == "1234567"

    key1 = "1234567"
    key2 = "1243567"
    common_part: str = get_common_key_part(key1, key2)
    assert common_part == "12"

    key1 = "234567"
    key2 = "1234567"
    common_part: str = get_common_key_part(key1, key2)
    assert common_part == ""

    key1 = ""
    key2 = "1234567"
    common_part: str = get_common_key_part(key1, key2)
    assert common_part == ""


def test_filter_keys_by_timestamps():
    """Test of filter_keys_by_timestamps."""

    keys = [
        b'1590495530_5e7cdae836954c50ac52f208302fb4cf',
        b'1590495531_5e7cdae836954c50ac52f208302fb4cf',
        b'1590495532_5e7cdae836954c50ac52f208302fb4cf',
        b'1590495533_5e7cdae836954c50ac52f208302fb4cf',
        b'1590495534_5e7cdae836954c50ac52f208302fb4cf',
        b'1590495535_5e7cdae836954c50ac52f208302fb4cf'
        ]
    assumption_list = [
            b'1590495531_5e7cdae836954c50ac52f208302fb4cf',
            b'1590495532_5e7cdae836954c50ac52f208302fb4cf',
            b'1590495533_5e7cdae836954c50ac52f208302fb4cf',
            b'1590495534_5e7cdae836954c50ac52f208302fb4cf'
            ]
    from_time = 1590495531
    to_time = 1590495534
    filtered: List[bytes] = filter_keys_by_timestamps(keys, from_time, to_time)
    assert filtered == assumption_list

    from_time = 1590495520  # lower timestamp values
    to_time = 1590495529
    filtered: List[bytes] = filter_keys_by_timestamps(keys, from_time, to_time)
    assert filtered == []

    from_time = 1590495536  # greater values
    to_time = 1590495539
    filtered: List[bytes] = filter_keys_by_timestamps(keys, from_time, to_time)
    assert filtered == []


def test_not_digits():
    """Test of not_digits"""

    assert not_digits("324356656", "324356656") is False
    assert not_digits("3243g56656", "3243g56656") is True
    assert not_digits("", "") is True


def test_get_domains_by_timestamps():
    """Test of get_domains_by_timestamps"""

    def get_domains_set(data):
        _domains = set()
        for domain_list in data:
            if domain_list:
                decoded_list = json.loads(domain_list)
                if decoded_list:
                    _domains.update(decoded_list)
        return _domains

    # 1) Add domains in Redis:
    timestamp1 = "1590495532"
    timestamp2 = "1590495533"
    key1 = f'{timestamp1}_5e7cdae836954c50ac52f208302fb4cf'
    key2 = f'{timestamp2}_5e7cdae836954c50ac52f208302fb4cf'
    domains_dict = {
            '1590495531_5e7cdae836954c50ac52f208302fb4cf': ["not_in_res.com"],
            key1: ["ya.ru", "funbox.ru"],
            key2: ["ya.ru", "google.com"],
            '1590495534_5e7cdae836954c50ac52f208302fb4cf': ["not_in_res.com"]
    }
    redis = RedisPool.get_instance()
    redis.flushdb()  # Clear redis for clean testing
    for key, value in domains_dict.items():
        redis.set(key, json.dumps(value))

    # 2) Receive domains by timestamps:
    domains1: Optional[List] = get_domains_by_timestamps(timestamp1, timestamp1)
    set1 = set(domains_dict[key1])
    assert get_domains_set(domains1) == set1  # for key1

    domains2: Optional[List] = get_domains_by_timestamps(timestamp2, timestamp2)
    set2 = set(domains_dict[key2])
    assert get_domains_set(domains2) == set2  # for key2

    domains3: Optional[List] = get_domains_by_timestamps(timestamp1, timestamp2)
    assert get_domains_set(domains3) == set1.union(set2)  # Combined 1 and 2

    domains4: Optional[List] = get_domains_by_timestamps("1600000000", "1800000000")
    assert domains4 is None  # No such domains

