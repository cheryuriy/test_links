import time

import requests

from links.redis_pool import RedisPool
from links.utils import get_domains_from_urls


BACKEND_URL = "http://localhost:8001"


def test_api_root():
    """Start page testing."""

    headers = {'Content-type': 'application/json'}
    response = requests.get(f"{BACKEND_URL}", headers=headers)
    assert response.ok is True
    data = response.json()
    assert data == {'Provide links': f'{BACKEND_URL}/visited_links',
                    'Receive domains': f'{BACKEND_URL}/visited_domains'}


def test_visited_links_and_visited_domains():
    """
    1) Testing visited_links
    2) Testing visited_domains:
    """

    links = {
        "links": [
            "https://ya.ru",
            "https://ya.ru?q=123",
            "funbox.ru",
            "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
        ]
       }
    result = {"ya.ru", "funbox.ru", "stackoverflow.com"}
    # 1) Testing visited_links:
    timestamp_start = int(time.time())
    redis = RedisPool.get_instance()
    redis.flushdb()  # Clear redis for clean testing
    response = requests.post(f"{BACKEND_URL}/visited_links", json=links)
    assert response.ok is True
    assert response.status_code == 201
    info = response.json()
    assert info.get("status") == "ok"
    timestamp_end = int(time.time())

    # 2) Testing visited_domains:
    response = requests.get(f"{BACKEND_URL}/visited_domains?from={timestamp_start}&to={timestamp_end}")
    assert response.ok is True
    assert response.status_code == 200
    data = response.json()
    assert set(data.get("domains")) == result
    # Additional function test:
    assert set(get_domains_from_urls(links["links"])) == result

