Web-приложение для учета посещенных сайтов... Docker, Django, DRF, Redis.
=====
**Установка:**

Для запуска вам понадобиться docker-compose.

Запустить:

docker-compose up -d

Посмотреть логи тестов:

docker-compose logs tests

Сервер:

http://localhost:8000/

**Описание:**

http://localhost:8000/visited_links

Принимает ссылки. {"links": [...]}

http://localhost:8000/visited_domains?from=1&to=1600000000

Отдаёт домены в заданный промежуток времени.