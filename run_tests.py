import subprocess
import time

import pytest


if __name__ == "__main__":
    process = subprocess.Popen(["python", "manage.py", "runserver", "0.0.0.0:8001"])
    time.sleep(1)  # enough for Django to start
    pytest.main()
    process.kill()
