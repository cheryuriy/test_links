from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = format_suffix_patterns([
    url(r'^$', views.api_root),  # Start page
    url(r'^visited_links$', views.LinksList.as_view(), name='visited_links'),
    url(r'^visited_domains$', views.DomainsList.as_view(), name='visited_domains')
    ])
