from logging import getLogger
from typing import List, Optional

import json
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView

from .redis_pool import RedisPool
from .serializers import LinksSerializer
from .utils import get_timestamp_and_unique_key, get_domains_from_urls, get_domains_by_timestamps, not_digits

logger = getLogger(__name__)


@api_view(['GET'])
def api_root(request, format=None):
    """Start page, representing all views."""

    urls = {
        'Provide links': reverse('visited_links', request=request, format=format),
        'Receive domains': reverse('visited_domains', request=request, format=format)
        }
    return Response(urls)


class LinksList(APIView):
    """
    Post list of links and save links in Redis with current timestamp.
    """

    def post(self, request, format=None):
        serializer = LinksSerializer(data=request.data)
        if serializer.is_valid():
            links = serializer.validated_data.get('links')
            if not links:
                return Response({"status": "No data."}, status=status.HTTP_400_BAD_REQUEST)

            # Generating unique_key for Redis so no data loss by racing condition:
            timestamp, unique_key = get_timestamp_and_unique_key()
            logger.info(f"Timestamp: {timestamp}, Links: {links}")
            domains: List[str] = get_domains_from_urls(links)
            logger.info(f"Timestamp: {timestamp}, Domains: {domains}")
            if not domains:
                return Response({"status": "No domains found."}, status=status.HTTP_400_BAD_REQUEST)

            redis = RedisPool.get_instance()
            redis.set(unique_key, json.dumps(domains))
            return Response({"status": "ok"}, status=status.HTTP_201_CREATED)
        return Response({"status": str(serializer.errors)}, status=status.HTTP_400_BAD_REQUEST)


class DomainsList(APIView):
    """
    Get list of domains between timestamps from and to.
    """

    def get(self, request, format=None):
        from_time = request.GET.get('from')
        to_time = request.GET.get('to')
        if not from_time or not to_time:
            return Response({"status": "Set from and to. Example: ?from=1&to=1600000000"}, status=status.HTTP_400_BAD_REQUEST)
        if not_digits(from_time, to_time):  # if contains not digits:
            return Response({"status": "from and to should be positive numbers."}, status=status.HTTP_400_BAD_REQUEST)

        data: Optional[List] = get_domains_by_timestamps(from_time, to_time)
        if not data:  # No matches in redis
            return Response({"domains": [], "status": "ok"}, status=status.HTTP_200_OK)
        domains = set()
        for domain_list in data:
            if domain_list:
                decoded_list = json.loads(domain_list)
                if decoded_list:
                    domains.update(decoded_list)
        response = {"domains": list(domains), "status": "ok"}
        return Response(response, status=status.HTTP_200_OK)
