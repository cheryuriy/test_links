from rest_framework import serializers


class LinksSerializer(serializers.Serializer):
    # Serializer for list of urls:
    links = serializers.ListField(child=serializers.CharField(required=True, max_length=150))
