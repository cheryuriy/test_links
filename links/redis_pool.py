import os
from typing import Optional

import redis


REDIS = {
    'host': os.environ.get("REDIS_HOST", '0.0.0.0'),
    'port': os.environ.get("REDIS_PORT", "6379"),
    'default_db': int(os.environ.get("REDIS_DB_NUM", 0)),
    'password': os.environ.get("REDIS_PASSWORD", 'redis12345'),
    'max_pool_size': int(os.environ.get("REDIS_POOL_SIZE", 10)),
}  # Redis is here because RedisPool should be the only connection for redis


class RedisPool:
    pool = dict()

    @classmethod
    def get_instance(cls, db: int = REDIS["default_db"]) -> Optional[redis.Redis]:
        """
            Singletone redis connection pool.

            Parameters
            ----------
            db : int
                Number of redis db

            Returns
            -------
            redis.Redis or None
                Sync Redis connection
            """

        if not REDIS:
            return None
        db = db if isinstance(db, int) else int(db)
        if not cls.pool.get(db):
            cls.pool[db] = redis.ConnectionPool(
                host=REDIS['host'], port=REDIS['port'],
                password=REDIS['password'], db=db,
                max_connections=REDIS['max_pool_size']
            )
        return redis.Redis(connection_pool=cls.pool[db], decode_responses=True)
