import re
import time
import uuid
from typing import List, Optional
from urllib.parse import urlparse

from .redis_pool import RedisPool

DIGITS = "0123456789"


def get_timestamp_and_unique_key() -> (int, str):
    """
        Generating unique_key for Redis so no data loss by racing condition.

        Returns
        -------
        int, str
            current timestamp, unique_key
    """

    timestamp = int(time.time())
    unique_key = f"{timestamp}_{uuid.uuid4().hex}"  # '1590495530_5e7cdae836954c50ac52f208302fb4cf'
    return timestamp, unique_key


def get_domains_from_urls(arr: List[str]) -> List[str]:
    """
        Get unique domains from array.

        Parameters
        ----------
        arr : List[str]
            Array of urls

        Returns
        -------
        List[str]
            Array of domains
    """

    domains_set = set()
    for url in arr:
        if not url:
            continue
        if not url.startswith('http://') and not url.startswith('https://'):
            length = len(url)
            i = 0
            while i < length:  # Skip not word's characters
                char = url[i]
                if re.match(r"\W", char):
                    i += 1
                else:
                    break
            if not url[i:]:  # Skip empty string
                continue
            url = 'http://' + url[i:]

        domain = urlparse(url).hostname
        if domain:
            domains_set.add(domain)
    return list(domains_set)


def get_common_key_part(key1: str, key2: str) -> str:
    """
        Finding common part in timestamps for future searching in Redis.

        Parameters
        ----------
        key1 : str
            First timestamp
        key2 : str
            Second timestamp

        Returns
        -------
        str
            common part of key1 and key2
    """

    len_key2 = len(key2)
    for i, char in enumerate(key1):
        if i >= len_key2:  # key2 ends
            return key1[:i]  # Common part
        if char != key2[i]:
            if i == 0:
                return ''  # Nothing common
            return key1[:i]  # Common part
    return key1  # key1 == key2


def filter_keys_by_timestamps(keys: List[bytes], from_time: int, to_time: int) -> List[bytes]:
    """
        Filtering keys by it timestamp: from_time <= timestamp <= to_time

        Parameters
        ----------
        keys : List[str]
            List of keys in Redis
        from_time : int
            Timestamp
        to_time : int
            Timestamp

        Returns
        -------
        List or None
            List of filtered keys for Redis
    """

    filtered = []
    for key in keys:
        # Extracting timestamp from '1590495530_5e7cdae836954c50ac52f208302fb4cf':
        timestamp = int(key.decode().split('_')[0])
        if from_time <= timestamp <= to_time:
            filtered.append(key)
    return filtered


def not_digits(*array: List[str]) -> bool:
    """
        Returns True if contains not digits, False if only digits.

        Parameters
        ----------
        array : List[str]
            List of strings to compare

        Returns
        -------
        bool
            True if contains not digits, False if only digits.
    """

    for string in array:
        if not string:
            return True
        for char in string:
            if char not in DIGITS:
                return True
    return False


def get_domains_by_timestamps(from_time: str, to_time: str) -> Optional[List]:
    """
        Get domains between timestamps from_time and to_time.

        Parameters
        ----------
        from_time : str
            Timestamp
        to_time : str
            Timestamp

        Returns
        -------
        List or None
            List of domains from Redis
    """

    # Calculate common pattern from timestamps:
    pattern: str = get_common_key_part(from_time, to_time) + '*'
    redis = RedisPool.get_instance()
    keys = []
    # Scanning keys by pattern:
    cur, new_keys = redis.scan(cursor=0, match=pattern)
    keys.extend(new_keys)
    while cur != 0:
        cur, new_keys = redis.scan(cursor=cur, match=pattern)
        keys.extend(new_keys)
    if not keys:  # No matches
        return None
    # Filtering keys by timestamp:
    filtered_keys: List[bytes] = filter_keys_by_timestamps(keys, int(from_time), int(to_time))
    if not filtered_keys:  # No keys between timestamps
        return None
    return redis.mget(filtered_keys)  # Receiving domains
